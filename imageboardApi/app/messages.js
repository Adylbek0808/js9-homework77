const path = require('path')
const express = require('express');
const multer = require('multer');
const { nanoid } = require('nanoid');
const database = require('../database');
const config = require('../config')

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({ storage })

router.get('/', async (req, res) => {
    const messages = await database.getItems();
    res.send(messages);
});

router.post('/', upload.single('image'), async (req, res) => {
    const message = req.body;
    if (req.file) {
        message.image = req.file.filename
    }
    await database.addItem(message);
    res.send(message);
});


module.exports = router;