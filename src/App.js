import { CssBaseline} from '@material-ui/core';
import React from 'react';
import Imageboard from './containers/Imageboard/Imageboard';


const App = () => {
  return (
    <>
      <CssBaseline />
      <Imageboard />
    </>
  );
}

export default App;
