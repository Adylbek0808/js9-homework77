import { Button, Grid, makeStyles, TextField, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import FileInput from '../../components/UI/Form/FileInput/FileInput';
import { createPost } from '../../store/actions/actions';

const useStyles = makeStyles(theme => ({
    formStyle: {
        margin: theme.spacing(4, 0, 12),
        border: "1px solid blue",
        borderRadius: '10px',
        padding: theme.spacing(2)
    }
}))




const NewMessage = () => {
    const [postInfo, setPostInfo] = useState({
        author: '',
        message: '',
        image: ''
    });
    const dispatch = useDispatch()

    const submitFormHandler = e => {
        // e.preventDefault();   Немного костляво, чтобы страница перезагружалась)
        const formData = new FormData();
        const datetime = new Date().toLocaleString()
        Object.keys(postInfo).forEach(key => {
            formData.append(key, postInfo[key]);
        });
        formData.append('datetime',datetime )
        dispatch(createPost(formData));

    };
    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setPostInfo(prevState => ({
            ...prevState,
            [name]: value
        }));
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setPostInfo(prevState => ({
            ...prevState,
            [name]: file
        }))
    }

    const classes = useStyles();
    return (
        <form className={classes.formStyle} onSubmit={submitFormHandler}>
            <Grid item container direction="column" spacing={2}>
                <Grid item xs>
                    <Typography variant="h6">Create new post</Typography>
                </Grid>
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        fullWidth
                        label="Author"
                        name="author"
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        fullWidth
                        multiline
                        rows={3}
                        label="Message"
                        name="message"
                        onChange={inputChangeHandler}
                        required
                    />
                </Grid>
                <Grid item xs>
                    <FileInput
                        name="image"
                        label="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Send</Button>
                </Grid>

            </Grid>
        </form>
    );
};

export default NewMessage;