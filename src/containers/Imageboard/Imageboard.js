import { CircularProgress, Grid, makeStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import Message from '../../components/Message/Message';
import { useDispatch, useSelector } from 'react-redux';
import NewMessage from '../NewMessage/NewMessage';
import { fetchPosts } from '../../store/actions/actions';

const useStyles = makeStyles(theme => ({
    baseGrid: {
        margin: theme.spacing(0, 'auto'),
        width: '80%'
    },
    progress: {
        height: 200
    }

}))


const Imageboard = () => {
    const dispatch = useDispatch()
    const posts = useSelector(state => state.postsList)
    const loading = useSelector(state => state.postsLoading)
    const classes = useStyles();

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={5} className={classes.baseGrid}>
            <h1>
                My imageboard
            </h1>
            {loading ? (
                <Grid container justify="center" alignItems="center" className={classes.progress}>
                    <Grid item>
                        <CircularProgress />
                    </Grid>
                </Grid>
            ) : posts.map(post => (
                <Message
                    key={post.id}
                    author={post.author}
                    datetime={post.datetime}
                    image={post.image}
                    message={post.message}
                />
            ))}
            <NewMessage />
        </Grid>
    );
};

export default Imageboard;