import axiosApi from "../../axiosApi";

export const FETCH_POSTS_REQUEST = "FETCH_POSTS_REQUEST";
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS";
export const FETCH_POSTS_FAILURE = "FETCH_POSTS_FAILURE";


export const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS";


export const fetchPostsRequest = () => ({ type: FETCH_POSTS_REQUEST });
export const fetchPostsSuccess = posts => ({ type: FETCH_POSTS_SUCCESS, posts });
export const fetchPostsFailure = () => ({ type: FETCH_POSTS_FAILURE });

export const createPostSuccess = () => ({ type: CREATE_POST_SUCCESS });


export const fetchPosts = () => {
    return async dispatch => {
        try {
            dispatch(fetchPostsRequest());
            const response = await axiosApi.get('/messages');
            console.log(response.data)
            dispatch(fetchPostsSuccess(response.data))
        } catch (e) {
            dispatch(fetchPostsFailure());
            console.log('Could not fetch products')
        }
    }
}

export const createPost = postData => {
    return async dispatch => {
        await axiosApi.post('/messages', postData);
        dispatch(createPostSuccess());
    }
}