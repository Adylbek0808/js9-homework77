
import { FETCH_POSTS_FAILURE, FETCH_POSTS_REQUEST, FETCH_POSTS_SUCCESS } from '../actions/actions';

const initialState = {
    postsList:[],
    postsLoading: false
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_POSTS_REQUEST:
            return { ...state, postsLoading: true }
        case FETCH_POSTS_SUCCESS:
            return { ...state, postsLoading: false, postsList: action.posts };
        case FETCH_POSTS_FAILURE:
            return { ...state, postsLoading: false }
        default:
            return state
    }
}

export default reducer;